## *Coding for Humanities*
### my expectations for that course
#### by Fabiana Wieland


[before and after - just another progress pic](http://devcv.me/wp-content/uploads/2013/01/rMind.jpg)

Earlier this day it would have been great to be a cheetah. Not for staying in the same position for 8 hours, but to run away from this computer science stuff.

But now, after intensely consulting my old friend **'Mr. Google'**, I feel:

* **great**
* **motivated**
* **proud**
* **tired but happy**

Just like the guy below the cheetah;)

My former expectation for that course was:

**hopefully not attract attention by running away screaming and crying**

My present expectations for that course are:

1. **learn and understand the basics of coding**
2. **develop unknown abilities**
3. **pass this course**
4. **become a so called 'nerd';)**




 

