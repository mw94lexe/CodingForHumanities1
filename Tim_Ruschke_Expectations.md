###First Assignment:
_

##Expectations of the Course

As I chose "Introduction to the Digital Humanities" as part of the "Wahlbereich der Geistes- und Soyialwissenschaften," I would like to get a general look into the subject and figure out if I would like to continue the subject in the future. In doing so, I expect:

* to learn coding and skills which supplement my major (my major being [American Studies](https://americanstudies.uni-leipzig.de/))
* to improve my general coding skills and learn how to keep up with future coding advancements
* outside of those broad goals, I have little particular expectations due to my knowledge on the subject being rather limited

Mainly I just hope it's manageable.


