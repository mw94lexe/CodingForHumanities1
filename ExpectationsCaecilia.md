# Coding for Humanities
My name is **Caecilia**. I'm actually a MA student in [theatre studies] (http://theaterwissenschaft.gko.uni-leipzig.de/). I started this second study to improve my researching methods in video game studies, which I've specialized on. 

## My Expectations of this Course: 
* learning about the coding basics/ methods of Digital Humanities
* learning how to work in groups (- in some kind of way my blind spot)
* broaden my methodical knowledge
